Source: nexus
Priority: optional
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders:
 Stuart Prescott <stuart@debian.org>
Build-Depends:
 cmake,
 debhelper-compat (= 12),
 default-jdk,
 doxygen,
 libhdf4-dev,
 libhdf5-dev,
#  libmxml-dev,
 libxml2-dev,
 pkg-config,
Standards-Version: 4.6.2
Section: science
Vcs-Browser: https://salsa.debian.org/science-team/nexus/
Vcs-Git: https://salsa.debian.org/science-team/nexus.git/
Homepage: https://github.com/nexusformat/code
Rules-Requires-Root: no

Package: libnexus1
Section: libs
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Homepage: http://www.nexusformat.org/
Description: NeXus scientific data file format - runtime libraries
 NeXus is a common data format for neutron, X-ray, and muon science. It
 is being developed as an international standard by scientists and
 programmers representing major scientific facilities in Europe, Asia,
 Australia, and North America in order to facilitate greater cooperation
 in the analysis and visualization of neutron, X-ray, and muon data.
 .
 This is the package containing the runtime libraries.

Package: libnexus-dev
Section: libdevel
Architecture: any
Depends:
 libhdf4-dev,
 libhdf5-dev,
 libmxml-dev,
 libnexus1 (= ${binary:Version}),
 ${misc:Depends}
Homepage: http://www.nexusformat.org/
Description: NeXus scientific data file format - development libraries
 NeXus is a common data format for neutron, X-ray, and muon science. It
 is being developed as an international standard by scientists and
 programmers representing major scientific facilities in Europe, Asia,
 Australia, and North America in order to facilitate greater cooperation
 in the analysis and visualization of neutron, X-ray, and muon data.
 .
 This is the package containing the development libraries.

Package: libnexus-java
Section: java
Architecture: all
Depends:
 libnexus-jni,
 ${misc:Depends},
Breaks: libnexus0-java
Replaces: libnexus0-java
Homepage: http://www.nexusformat.org/
Description: NeXus scientific data file format - java libraries
 NeXus is a common data format for neutron, X-ray, and muon science. It
 is being developed as an international standard by scientists and
 programmers representing major scientific facilities in Europe, Asia,
 Australia, and North America in order to facilitate greater cooperation
 in the analysis and visualization of neutron, X-ray, and muon data.
 .
 This is the package containing the java libraries.


Package: libnexus-jni
Section: java
Architecture: any
Depends:
 libnexus1 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Breaks: libnexus0-java
Replaces: libnexus0-java
Homepage: http://www.nexusformat.org/
Description: NeXus scientific data file format - JNI library
 NeXus is a common data format for neutron, X-ray, and muon science. It
 is being developed as an international standard by scientists and
 programmers representing major scientific facilities in Europe, Asia,
 Australia, and North America in order to facilitate greater cooperation
 in the analysis and visualization of neutron, X-ray, and muon data.
 .
 This is the package containing the JNI libraries.

Package: nexus-tools
Architecture: any
Depends:
 libnexus1 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Homepage: http://www.nexusformat.org/
Description: NeXus scientific data file format - applications
 NeXus is a common data format for neutron, X-ray, and muon science. It
 is being developed as an international standard by scientists and
 programmers representing major scientific facilities in Europe, Asia,
 Australia, and North America in order to facilitate greater cooperation
 in the analysis and visualization of neutron, X-ray, and muon data.
 .
 This is the package containing some applications for reading and writing
 NeXus files.
